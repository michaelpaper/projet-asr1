# README

## Assembleur

La version antérieure de l'assembleur fournie dans le rendu 2 se trouve dans le fichier asm.py.old.  
L'assembleur actuel se trouve dans le répertoire asm.src. 
C'est une version réécrite qui nous a permi de rajouter des fonctionnalités à ce dernier.
Le fichier utils.py contient des fonctions auxiliares qui sont utilisées dans tout le programme.
Le fichier parser.py implémente la classe d'un parseur de fichier source assembleur.
Le fichier assembler.py assemble la liste de jeton renvoyée par le parseur.
Le fichier logger.py implémente la classe d'un très sommaire système de logging imbriqué.

L'assembleur implémente l'ISA ainsi que le sucre syntaxique attendu dans le DM.
Mais aussi un préprocesseur simple composé des commandes suivantes:  

`#const <nom> <valeur>:`  
Charge dans l'environment de l'assembleur la constante de nom <nom> et de valeur <valeur>. 
Pour pouvoir ensuite l'utiliser il suffit d'écrite `@nom` à la place d'une valeur numérique dans toutes les
mnémoniques qui attendent des valeurs numériques.  
Ex:  

    #const toto 0x25     
    ld2 r2 @toto

se traduit en:  

    ld2 r2 25
  
`#include <fichier>:`
Assemble le fichier source de chemin <fichier> et insére sa version code machine à l'endroit du include  
`#loadbin <fichier>:`  
Charge à l'endroit du loadbin le contenu du fichier de chemin <fichier>. Le contenu de <fichier> est prié d'être un chaîne hexadecimale.

Aide de l'assembleur:  

    usage: asm.py [-h] [-v] [-s] [-o OUTPUT] filename

    This is the assembler for the ASR2019 processor @ ENS-Lyon

    positional arguments:
    filename              name of the source file. "python asm.py toto.s" assembles
                            toto.s into toto.obj

    optional arguments:
    -h, --help            show this help message and exit
    -v, --verbose         increase output verbosity
    -s, --saveLineFormat  conserves line return in the compiled object file
    -o OUTPUT, --output OUTPUT
                            optional output filename

## Simulateur
* a besoin de la SDL2 (sous linux: sudo apt-get install libsdl2-dev pour l'installer)
* Compilation:
cd simu.src; make



## Fractales

Nous avons implémenté une opération de multiplication de deux nombres
à virgule fixe, ce qui nous a permis de tracer des fractales.

### Mandelbrot

Se trouve dans le fichier `ASM/mandelbrot.s`. En noir, les points de
l'ensemble, en dégradé orange, les points pour lesquels la suite de
Mandelbrot diverge (plus ils sont clairs plus ils prennent de temps à diverger).

### Julia

Se trouve dans le fichier `ASM/julia.s`. En noir, les points de
l'ensemble, en dégradé orange, les points pour lesquels la suite
diverge.
Les ensembles de Julia sont indicés par un nombre complexe c. Au début
du fichier sont définies les constantes `PARTIE_REELLE` et
`PARTIE_IMAGINAIRE` qui sont la partie réelle et la partie imaginaire
du complexe c dont on veut afficher l'ensemble de Julia correspondant.
