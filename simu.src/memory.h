#ifndef memory_hpp
#define memory_hpp
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <string>
#include <list>
#include <cstdlib>
#include <stdint.h>
#include <stdio.h>

static const uint32_t MEM_SIZE = 1<<20; // size in nibbles
static const uint32_t START_PC = 1<<10;

// more constant in screen.h

class Memory {
 public:

	Memory();
	~Memory();
	/** returns a uint32_t but only the lower 4 bits are relevant  */
	uint32_t read(uint32_t address);

	/** writes the lower 4 bits of data at address */
	void write(uint32_t address, uint32_t data);

	int fill_with_obj_file(std::string filename, uint32_t pos=1024);

 private:
	// MEM_SIZE is in nibbles, so we divide it by 8 to allocate an array of 32-bit integers
	uint32_t mem_array[MEM_SIZE>>3]; 

};


#endif
