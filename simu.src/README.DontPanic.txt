Required packages to install:
g++ libsdl2-dev

To compile:
make

In principle you only have to edit processor.cpp.

Later you may want to add methods to memory.cpp, e.g. to initialize some part of the memory.

In screen.cpp you have the part that maps the graphics and the keyboard to the memory of your simulator.

