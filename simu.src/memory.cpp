#include "memory.h"

Memory::Memory(){}
Memory::~Memory(){}


uint32_t Memory::read(uint32_t address)
{
	if(address>MEM_SIZE)
		{
			std::cerr<<"Illegal memory access: address " << address << " out of bound" << std::endl;
			return 0;
		}
	else {
		uint32_t address_of_word = address>>3;
		uint32_t nibble_index = address & 0x00000007;
		
		uint32_t val;
		val = mem_array[address_of_word] ;
		val = val >> (4*nibble_index);
		val = val & 0x0000000F;
		return val;
	}
}


void Memory::write(uint32_t address, uint32_t data)
{
	if(address>MEM_SIZE)
		{
			std::cerr<<"Illegal memory access: address " << address << " out of bound" << std::endl;
		}
	else {
		// read 32 bits, then modify one nibble, then write back
		uint32_t address_of_word = address>>3;
		uint32_t nibble_index = address & 0x00000007;
		
		uint32_t oldval, d, mask;
		// get 32 bits
		oldval = mem_array[address_of_word] ;
		
		// build a mask to zero only the nibble we want to write;
		mask = ~(0xF << (4*nibble_index));
		oldval = oldval & mask;
		
		// shift the nibble to write in place
		d = (data & 0xF) << (4*nibble_index);
		
		// add it in place and write back
		mem_array[address_of_word] = oldval+d;
	}
}



int Memory::fill_with_obj_file(std::string filename, uint32_t pos) {
	char c;
	uint32_t address;
	std::fstream fin(filename, std::fstream::in);
	std::cerr << "loading... " << std::endl;
	address = START_PC;
	 if(fin)
		{
			// Absolutely no check here; we read the hexadecimal byte by byte because this is C++
			while (fin >> c)
				{
					// hex conversion -- I'm sure it could be done by std
					uint32_t n;
					if ('0' <= c && c<='9')
						n=c-'0';
					else if ('A' <= c && c<='F')
						n=c-'A'+10;
					else if ('a' <= c && c<='f')
						n=c-'a'+10;
					else {
						std::cerr << "read illegal hex char: " << c  << std::endl;
						exit(1);
					}
					// std::cout << "read " << std::hex << c << " dec: " << std::dec << n << std::endl;
					write(address, n);
					address++;
				}
		fin.close();
		std::cerr << " done, read " << address - START_PC << " nibbles" << std::endl;

		//		for (uint32_t i=START_PC>>3; i<(address>> 3); i++)
		//     	std::cout << std::hex << std::setw(8) << std::setfill('0') <<  mem_array[i] << std::endl;
		return 0;
	}
	else
		{
			std::cerr << " error : can't open file"<< std::endl;
			return 1;
	}
}
