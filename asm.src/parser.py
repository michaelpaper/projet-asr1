"""
    Parsing util class
"""
# -*- coding: UTF-8 -*-
import re
from utils import error
from logger import Logger

"""
    Token are represented by couple (id, args) where is an
    integer representing the id of the token and args is a tuple
    representing the token arguments
"""
PREPROC_INSTR = 0    # args = (name : str)
LABEL_DECL    = 1    # args = (name : str)
LABEL_USE     = 2    # args = (name : str)
MEMNO         = 3    # args = (name : str)
CONST_INSERT  = 4    # args = (size : int)
CONST_ALIAS   = 5    # args = (name : str)
REG_ID        = 6    # args = (id : char)
NUMBER        = 7    # args = (isHex : bool, litteral : str)
RANDOM_STRING = 9    # args = (str : str)

class Parser(object):

    """
        Parse & test if a given file has a valid syntax
        and transforms it into a list of tokens 
    """
    def __init__(self, filepath, debugEnable = True):
        self.identifier = re.compile("^[^\d\W]\w*\Z")
        self.hexliteral = re.compile("0[xX][0-9a-fA-F]+")
        self.file = open(filepath)
        self.filepath = filepath
        self.logger = Logger(debugEnable, "[DEBUG@PARSER]", 0)
        self.lineNumber = 0
        self.line = ""
        self.declaredLabels = set()

    def validIdentifier(self, str):
        return (re.match(self.identifier, str) is not None)

    def validHexNumber(self, str):
        return (re.match(self.hexliteral, str) is not None)

    def validDecNumber(self, str):
        try:
            int(str)
            return True
        except:
            return False

    def parse(self):
        self.lineNumber = 0; self.file.seek(0)
        parsedProgram = []
        # Start line parsing
        for line in self.file.readlines():
            self.line = line
            # if there is a comment, get rid of it
            index = line.find(";")
            if index != -1:
                line = line[:index]
            # split the non-comment part of the line into tokens (thanks Stack Overflow) 
            rawTokens = [x.strip() for x in re.findall('[\S]+', line)] # \S means: any non-whitespace
            rawTokenCount = len(rawTokens)
            self.debug("Line %d as raw tokens: " % self.lineNumber, rawTokens)

            tokens = []; isFirstToken = True; nextToMemno = False
            for rawToken in rawTokens:
                tokenID, args = self.parseToken(rawToken, isFirstToken, nextToMemno)
                nextToMemno =  (tokenID == MEMNO)
                tokens.append( (tokenID, args) )
                isFirstToken = False

            self.debug("Line %d has been parsed as" % self.lineNumber, tokens)
            self.lineNumber += 1

            if (tokens):
                parsedProgram.append( (self.lineNumber, tokens) )

        return parsedProgram

    def parseToken(self, rawToken, isFirstToken, nextToMemno):
        # Preproc test
        if (rawToken.startswith('#')):
            if ( self.validIdentifier(rawToken[1:]) ):
                return( (PREPROC_INSTR, rawToken[1:].lower()) )
            else:
                self.parseError("Ill-formatted preprocessing instructions")
                
        # Label declaration test
        if (rawToken.endswith(':')):
            if ( self.validIdentifier(rawToken[:-1]) ):
                if not ((len(rawToken) >= 2) 
                    and rawToken[0] in ['r, R'] and rawToken[1] in "0123456789ABCDEFabcdef"):
                    self.declaredLabels.add( rawToken[:-1] )
                    return( (LABEL_DECL, (rawToken[:-1])) )
                else:
                    self.parseError("Cannot use register identifier as label name")
            else:
                self.parseError("Ill-formatted label declaration")

        # Constant alias test
        if (rawToken.startswith('@')):
            if ( self.validIdentifier(rawToken[1:]) ):
                return( (CONST_ALIAS, (rawToken[1:])) )
            else:
                self.parseError("Ill-formatted constant alias declaration")

        # Constant insertion test
        if (rawToken.startswith('.const')):
            if ( self.validIdentifier(rawToken[1:]) 
                and rawToken[6:] in ['4', '8', '16', '32']):
                return( (CONST_INSERT, int(rawToken[6:]) // 4) )
            else:
                self.parseError("Ill-formatted constant insertion declaration")

        # Register id test
        if (rawToken.lower().startswith('r') 
            and len(rawToken) == 2 and rawToken[1] in "0123456789ABCDEFabcdef"):
            return( (REG_ID, rawToken[1].upper()) )

        # Memno test
        if ( isFirstToken and self.validIdentifier(rawToken) ):
            return( (MEMNO, rawToken.lower()) )

        # Label usage test
        if ( rawToken in self.declaredLabels and self.validIdentifier(rawToken) ):
            return( (LABEL_USE, rawToken) )

        # Numeric constant test
        if (self.validHexNumber(rawToken)):
            return( (NUMBER, (True, rawToken.upper()[2:])) )
        elif (self.validDecNumber(rawToken)):
            return( (NUMBER, (False, rawToken)) )

        # Defaut case
        return( (RANDOM_STRING, rawToken) )

    def debug(self, *args):
        self.logger.debug(args)

    def parseError(self, msg):
        error("Syntax error at line %d in %s: %s \n >>> %s" % 
            (self.lineNumber, self.filepath, msg, self.line))

    def cleanup(self):
        self.file.close()