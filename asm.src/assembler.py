"""
    Assembler class
"""
# -*- coding: UTF-8 -*-
from parser import *
from utils import *
from os.path import dirname, join

class Assembler(object):

    def __init__(self, filepath, saveLineFormat = False, debugEnable = True, startingADDR = 1024, prefixSpaces = 0, muteInfo = False):
        self.startingADDR = startingADDR
        self.current_address = startingADDR
        self.labels = {}
        self.environment = {}
        self.filepath = filepath
        self.saveLineFormat = saveLineFormat
        self.logger = Logger(debugEnable, "[DEBUG@ASSEMBLER]", prefixSpaces, muteInfo)
        self.parsedProg = []
        self.lineNumber = 0
        self.compiledCode = ""
        self.instrNumber = 0
        self.avgInstructionSize = 0.0
    
    def debug(self, *args):
        self.logger.debug(args)

    def asmError(self, msg):
        error("Assembly error at line %d in %s: \n   %s" % 
            (self.lineNumber, self.filepath, msg))

    def relative_filepath(self, path):
       source_dirname = dirname(self.filepath)
       return join(source_dirname, path)

    def assemble(self):
        parser = Parser(self.filepath, self.logger.debugEnable)
        parser.parse()
        self.parsedProg = parser.parse()
        # First run to compute labels
        self.assemble_pass(True, False)
        # Debugging labels
        self.debug("Labels: " + str(self.labels))
        # Real pass to generate compiled code
        self.assemble_pass(False, True)
        self.avgInstructionSize = (1.0*(self.current_address-self.startingADDR) / (1.0*self.instrNumber))
        parser.cleanup()

    def assemble_pass(self, ignore_missing_labels, real_run):
        self.lineNumber = 0
        self.current_address = self.startingADDR
        for (self.lineNumber, tokens) in self.parsedProg:

            instruction_encoding = ""
            instruction_ignored = False
            self.debug("Processing line %d: " % self.lineNumber, tokens) # just to get rid of the final newline

            instrType, args = tokens[0]    # name used to be called opcode in the previous version
            token_count = len(tokens)

            if (instrType == LABEL_DECL): # Label declaration support
                name = args
                self.debug(" ...found label declaration: " + name + "  at address " + str(
                    self.current_address))
                if (name in self.labels and ignore_missing_labels):
                    self.asmError("error: this label was already defined, at address " + 
                        str(self.labels[name]) + ", " + name)
                self.labels[name] = self.current_address
                instruction_ignored = True

            elif (instrType == MEMNO):  # Memnomics supprot
                name = args
                # Jump instructions
                if name == "j" and token_count==2:
                    instruction_encoding = "0" + self.asm_offset_signed(tokens[1], ignore_missing_labels)
                if name == "jt" and token_count==2:
                    instruction_encoding = "1" + self.asm_offset_signed(tokens[1], ignore_missing_labels)
                if name == "jf" and token_count==2:
                    instruction_encoding = "2" + self.asm_offset_signed(tokens[1], ignore_missing_labels)
                
                # Arithmetics
                if name == "add" and token_count == 3:
                    instruction_encoding = "3" + self.asm_reg(tokens[1]) + self.asm_reg(tokens[2])
                if name == "sub" and token_count==3:
                    instruction_encoding = "4" + self.asm_reg(tokens[1]) + self.asm_reg(tokens[2])
                # copy
                if name == "copy" and token_count==3:
                    instruction_encoding = "5" + self.asm_reg(tokens[1]) + self.asm_reg(tokens[2])

                # Shift handling
                if name[:5] == "shift" and token_count == 2:
                    if name[5] in ['l', 'r']:
                        d = int(name[5] == 'r')
                        n = name[6:]
                        n_table = ['1', '2', '3', '4', '8', '12', '16', '24']
                        if n in n_table:
                            shift_mode = HEXTABLE[d*8 + n_table.index(n)]
                            instruction_encoding = "6" + shift_mode + self.asm_reg(tokens[1])
                

                # comparisons
                if name == "isequal" and token_count==3:
                    instruction_encoding = "8" + self.asm_reg(tokens[1]) + self.asm_reg(tokens[2])
                if name == "isgt" and token_count==3:
                    instruction_encoding = "9" + self.asm_reg(tokens[1]) + self.asm_reg(tokens[2])

                # Memory instructions
                if name[:2] == "mr" and token_count == 3:
                    if name[2] in ['1', '2', '3', '4', '5', '6', '7', '8']:
                        X = HEXTABLE[int(name[2])-1] # pred of hexdecimal digit
                        instruction_encoding = "A" + X + self.asm_reg(tokens[1]) + self.asm_reg(tokens[2])
                if name[:3] == "mru" and token_count == 3:
                    if name[3] in ['1', '2', '3', '4', '5', '6', '7', '8']:
                        X = HEXTABLE[int(name[3])-1 + 8] # pred of hexdecimal digit
                        instruction_encoding = "A" + X + self.asm_reg(tokens[1]) + self.asm_reg(tokens[2])
                if name[:2] == "mw" and token_count == 3:
                    if name[2] in ['1', '2', '3', '4', '5', '6', '7', '8']:
                        X = HEXTABLE[int(name[2])-1] # pred of hexdecimal digit
                        instruction_encoding = "B" + X + self.asm_reg(tokens[1]) + self.asm_reg(tokens[2])
                if name[:3] == "mwu" and token_count == 3:
                    if name[3] in ['1', '2', '3', '4', '5', '6', '7', '8']:
                        X = HEXTABLE[int(name[3])-1 + 8] # pred of hexdecimal digit
                        instruction_encoding = "B" + X + self.asm_reg(tokens[1]) + self.asm_reg(tokens[2])

                # syscall
                if name == "syscall" and token_count == 2:
                    instruction_encoding = "C" + self.asm_syscall_address(tokens[1])

                # push
                if name[:4] == "push" and token_count == 3:
                    if name[4] in ['1', '2', '3', '4', '5', '6', '7', '8']:
                        X = HEXTABLE[int(name[4])-1 + 8] # pred of hexdecimal digit
                        instruction_encoding = "C" + X + self.asm_reg(tokens[1]) + self.asm_reg(tokens[2])

                # call allows user to use label
                if name == "call" and token_count == 2:
                    instruction_encoding = "D" + self.asm_call_address(tokens[1], ignore_missing_labels)
                # ret
                if name == "ret" and token_count == 1:
                    instruction_encoding = "E"

                # Bitwise operations
                if name == "not" and token_count == 3:
                    instruction_encoding = "F0" + self.asm_reg(tokens[1]) + self.asm_reg(tokens[2])
                if name == "and" and token_count == 3:
                    instruction_encoding = "F1" + self.asm_reg(tokens[1]) + self.asm_reg(tokens[2])         
                if name == "or" and token_count == 3:
                    instruction_encoding = "F2" + self.asm_reg(tokens[1]) + self.asm_reg(tokens[2])
                if name == "xor" and token_count == 3:
                    instruction_encoding = "F3" + self.asm_reg(tokens[1]) + self.asm_reg(tokens[2])

                # Constant loading
                if name == "ld2" and token_count == 3:
                    instruction_encoding = "F4" + self.asm_reg(tokens[1]) + self.asm_constant_ld2(tokens[2])
                if name == "ld4" and token_count == 3:
                    instruction_encoding = "F5" + self.asm_reg(tokens[1]) + self.asm_constant_ld4(tokens[2])
                if name == "ld8" and token_count == 3:
                    instruction_encoding = "F6" + self.asm_reg(tokens[1]) + self.asm_constant_ld8(tokens[2], ignore_missing_labels)

            elif instrType == CONST_INSERT:  # Constant insertion suppport
                size = args
                if token_count == 2:
                    tokenID, args = tokens[1]
                    if tokenID == NUMBER:
                        isHex, lit = args
                        if isHex:
                            lit = '0x' + lit
                        instruction_encoding = litteral_unifier(lit,
                            miochNB=size, signedResult=False, errHeader=".const" + str(4*size))

            elif instrType == PREPROC_INSTR:  # Preprocessing directives support
                name = args
                if name == "const" and token_count == 3:  # Constant aliasing support
                    constID = ''
                    if tokens[1][0] == RANDOM_STRING:
                        constID = tokens[1][1]
                    else:
                        self.asmError("Unvalid constant identifier")
                    tokenID, args = tokens[2]
                    if tokenID == NUMBER:
                        isHex, val = args
                        if isHex:
                            self.environment[constID] = '0x' + val
                        else:
                            self.environment[constID] = val
                    else:
                        self.asmError("Unvalid litteral for constant alias")
                    instruction_ignored = True
                elif name == "include" and token_count == 2:
                    tokenID, args = tokens[1]
                    if tokenID == RANDOM_STRING:
                        filename = self.relative_filepath(args)
                        if (real_run):
                            self.logger.info("+ Compiling subfile \"%s\": " % args)
                        nested_asm = Assembler(filename, False, 
                            self.logger.debugEnable, startingADDR = self.current_address, prefixSpaces = self.logger.prefixSpaces + 3, muteInfo = not real_run)
                        nested_asm.assemble()
                        instruction_encoding = nested_asm.compiledCode
                        self.instrNumber += nested_asm.instrNumber - 1
                        if (real_run):
                            self.logger.info(" Instructions number:  ", nested_asm.instrNumber)
                            self.logger.info(" Avg instruction size: ", nested_asm.avgInstructionSize)
                    else:
                        self.asmError("Unvaling source file")
                elif name == "loadbin" and token_count == 2:
                    tokenID, args = tokens[1]
                    if tokenID == RANDOM_STRING:
                        filename = self.relative_filepath(args)
                        if (real_run):
                            self.logger.info("Loading binary file: \"%s\"" % args)
                        file = open(filename, 'r')
                        lines = [line.strip() for line in file.readlines()]
                        file.close()
                        instruction_encoding = ''.join(lines)
                        # On considère que le coût d'un chargement binaire est celui qu'il faudrait pour l'écrire dans la mémoire
                        self.instrNumber += len(instruction_encoding) // 128
                    else:
                        self.asmError("Unvalid binary file")
                else:
                    self.asmError("Unvalid preprocessing directive: " + name)

            
            """
                State updating
            """
            # If the line wasn't assembled:
            if instruction_ignored:
                pass
            elif instruction_encoding == "":
                self.asmError("Don't know what to do with: " + str(tokens))
            else:
                instr_size = len(instruction_encoding)
                # Debug output
                self.debug(" ... @ " + str(self.current_address) + "  :  " + instruction_encoding )
                self.current_address += instr_size
                if (real_run):
                    self.compiledCode += instruction_encoding
                    if (self.saveLineFormat):
                        self.compiledCode += '\n'
                    self.instrNumber += 1

    # All the asm_xxxx functions are helper functions that parse an operand and convert it into its hex encoding.
    # Feel free to add more
    def asm_reg(self, token):
        tokenID, args = token
        if tokenID == NUMBER:
            _, val = args
            if val == '0' or val == '1':
                return val
            else:
                self.asmError("Can only use '0' or '1' instead of register id")
        elif tokenID == REG_ID:
            return args

    def asm_offset_signed(self, token, ignore_missing_labels):
        tokenID, args = token
        if tokenID == NUMBER:
            isHex, lit = args
            if isHex:
                lit = '0x' + lit
            return litteral_unifier(lit, errHeader = "asm_offset_signed")
        elif tokenID == LABEL_USE:
            label = args
            if (not ignore_missing_labels):
                if (label in self.labels):
                    return padded_hex(self.labels[label] - self.current_address + 256, 2)
                else:
                    self.asmError("Missing label error: " + label)
            else:
                return '00'

    def asm_syscall_address(self, token):
        tokenID, args = token
        lit = ''
        if tokenID == CONST_ALIAS:
            if (args in self.environment):
                lit = self.environment[args]
            else:
                self.asmError("Could not found constant %s in environment" % args)
        elif tokenID == NUMBER:
            isHex, lit = args
            if isHex:
                lit = '0'     
        else:
            self.asmError("Unvalid syscall argument: " + str(token))
        try:
            val = int(lit)
            if val >= 0 and val <= 127:
                return padded_hex(val, 2)[::-1]
            else:
                error("syscall adress out of range " + str(token))
        except:
            error("invalid syscall address: " + str(token))

    def asm_call_address(self, token, ignore_missing_labels = False):
        tokenID, args = token
        lit = ''
        if tokenID == CONST_ALIAS:
            if (args in self.environment):
                lit = self.environment[args]
            else:
                self.asmError("Could not found constant %s in environment" % args)
        elif tokenID == NUMBER:
            isHex, lit = args
            if isHex:
                lit = '0x' + lit
        elif tokenID == LABEL_USE:
            if (not ignore_missing_labels):
                if (args in self.labels):
                    return padded_hex(self.labels[args], 8)
                else:
                    self.asmError("Missing label error: " + args)
            else:
                lit = '00000000'
        else:
            self.asmError("Unvalid call argument: " + str(token))
        return litteral_unifier(lit, miochNB = 8, signedResult = False,
                    errHeader = "asm_call_adress")

    def asm_constant_ld2(self, token):
        tokenID, args = token
        lit = ''
        if tokenID == CONST_ALIAS:
            if (args in self.environment):
                lit = self.environment[args]
            else:
                self.asmError("Could not found constant %s in environment" % args)
        elif tokenID == NUMBER:
            isHex, lit = args
            if isHex:
                lit = '0x' + lit
        else:
            self.asmError("Unvalid ld2 argument: " + str(token))
        return litteral_unifier(lit, miochNB = 2, signedResult = True,
                    errHeader = "asm_constant_ld2")

    def asm_constant_ld4(self, token):
        tokenID, args = token
        lit = ''
        if tokenID == CONST_ALIAS:
            if (args in self.environment):
                lit = self.environment[args]
            else:
                self.asmError("Could not found constant %s in environment" % args)
        elif tokenID == NUMBER:
            isHex, lit = args
            if isHex:
                lit = '0x' + lit
        else:
            self.asmError("Unvalid ld4 argument: " + str(token))
        return litteral_unifier(lit, miochNB = 4, signedResult = True,
                    errHeader = "asm_constant_ld4")

    def asm_constant_ld8(self, token, ignore_missing_labels):
        tokenID, args = token
        lit = ''
        if tokenID == CONST_ALIAS:
            if (args in self.environment):
                lit = self.environment[args]
            else:
                self.asmError("Could not found constant %s in environment" % args)
        elif tokenID == NUMBER:
            isHex, lit = args
            if isHex:
                lit = '0x' + lit
        elif tokenID == LABEL_USE:
            if (not ignore_missing_labels):
                if (args in self.labels):
                    return padded_hex(self.labels[args], 8)
                else:
                    self.asmError("Missing label error: " + args)
            else:
                return '00000000'
        else:
            self.asmError("Unvalid ld8 argument: " + str(token))
        return litteral_unifier(lit, miochNB = 8, signedResult = True,
                    errHeader = "asm_constant_ld8")
