# -*- coding: UTF-8
"""
    Contains various helping functions and constants
"""
HEXTABLE = "0123456789ABCDEF" # global conversion table

def error(e):
    raise BaseException(e)

def padded_hex(val, miochNB):
    s = ""; size = 0
    while (val > 0):
        val,r = divmod(val, 16)
        s += HEXTABLE[r]
        size += 1
    for _ in range(miochNB - size):
        s += '0'
    return s[:miochNB]

def litteral_unifier(lit, miochNB = 2, signedResult = True, errHeader = "unknown"):
    """ converts a litteral in base 10 or 16 to the string 
    representation of the signed/unsigned integer it represents """
    m = 4 * miochNB
    try:
        val = 0
        # Base 10
        if not ((len(lit) >= 2) and lit[0] == '0' and lit[1] == 'x'):
            val = int(lit) 
        # Base 16
        else:
            val = int(lit, 16)
            if signedResult and val >= (1 << (m-1)):
                val -= (1 << m)
        if signedResult:
            if val >= -(1 << (m-1)) and val <= (1 << (m-1))-1:
                return padded_hex(val + (1 << m), miochNB)
            else:
                error(errHeader + ": litteral out of bound " + lit + ", " + str(val))
        else:
            if val >=0 and val <= (1 << m)-1:
                return padded_hex(val, miochNB)
            else:
                error(errHeader + ": litteral out of bound " + lit + ", " + str(val))
    except:
        error(errHeader + ": invalid litteral " + lit)