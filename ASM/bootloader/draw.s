  ;; prend en argument les coordonnées (r2,r3), (r4,r5) et une couleur r6
  ;; colorie le segment formé par les coordonnées (r2, r3) et (r4, r5) à la couleur representée r6
  ;; PROLOGUE
  push8 rf 0  ; save pcra
  push8 rf r2  ; x1
  push8 rf r3  ; y1
  ; r4 vaut x2
  ; r5 vaut y2
  ; r6 la couleur du segment
  push8 rf r7  ; dx
  push8 rf r8  ; dy
  push8 rf r9  ; e
  push8 rf ra  ; color buffer
      
  ;; CORPS  
  ; dx <- x2 - x1
  copy r7 r4
  sub r7 r2
  ; @test (dx == 0)    
  isequal r7 r0
  jf 12
  call draw_test1_true
  ; dx != 0
    ; @test (dx > 0)
    isgt r7 r0
    jt 12
    call draw_test2_false
    ; dx > 0		
      ; dy <- y2 - y1
      copy r8 r5
      sub r8 r3
      ; @test (dy == 0)
      isequal r8 r0
      jf 12
      call draw_test3_true
      ; dy != 0
        ; @test (dy > 0)
        isgt r8 r0
        jt 12
        call draw_test4_false
        ; dy > 0
          ; @test (dy > dx)
          isgt r8 r7
          jt draw_test5_true 
          ; dx >= dy 
            copy r9 r7 ; (e <- dx)
            add r7 r7  ; (dx <- 2 * dx)
            add r8 r8  ; (dy <- 2 * dy)
            
            draw_boucle1_start:
              copy ra r4
              copy r4 r6
              syscall 2
              copy r4 ra
              add r2 r1
              isequal r2 r4
              jf 12
              call draw_epilogue
              ; e <- e - dy
              sub r9 r8
              isgt r0 r9
              jf draw_boucle1_start
              add r3 r1
              add r9 r7
              j draw_boucle1_start

          ; dx < dy
          draw_test5_true:
            copy r9 r8 ; (e <- dy)
            add r7 r7  ; (dx <- 2 * dx)
            add r8 r8  ; (dy <- 2 * dy)

            draw_boucle2_start:
              copy ra r4
              copy r4 r6
              syscall 2
              copy r4 ra
              add r3 r1
              isequal r3 r5
              jf 12
              call draw_epilogue
              ; e <- e - dx
              sub r9 r7
              isgt r0 r9
              jf draw_boucle2_start
              add r2 r1
              add r9 r8
              j draw_boucle2_start

        ; dy < 0
        draw_test4_false:
          copy ra r7
          add ra r8
          ; test (dx + dy < 0)
          isgt ra r0
          jt draw_test6_true
          ; dx >= -dy
            copy r9 r7 ; (e <- dx)
            add r7 r7  ; (dx <- 2 * dx)
            add r8 r8  ; (dy <- 2 * dy)

            draw_boucle3_start:
              copy ra r4
              copy r4 r6
              syscall 2
              copy r4 ra
              add r2 r1
              isequal r2 r4
              jf 12
              call draw_epilogue
              ; e <- e + dy
              add r9 r8
              isgt r0 r9
              jf draw_boucle3_start
              sub r3 r1
              add r9 r7
              j draw_boucle3_start

          ; dx < -dy
          draw_test6_true:
            copy r9 r8 ; (e <- dy)
            add r7 r7  ; (dx <- 2 * dx)
            add r8 r8  ; (dy <- 2 * dy)

            draw_boucle4_start:
              copy ra r4
              copy r4 r6
              syscall 2
              copy r4 ra
              sub r3 r1
              isequal r3 r5
              jf 12
              call draw_epilogue
              ; e <- e + dx
              add r9 r7
              isgt r9 r0
              jf draw_boucle4_start
              add r2 r1
              add r9 r8
              j draw_boucle4_start

      ; dy == 0
      draw_test3_true:
        copy ra r4
        copy r4 r6
        syscall 2
        copy r4 ra
        add r2 r1
        isequal r2 r4
        jf 12
        call draw_epilogue
        jf draw_test3_true  

    ; dx < 0
    draw_test2_false:
      ; dy <- y2 - y1
      copy r8 r5
      sub r8 r3
      ; @test (dy == 0)
      isequal r8 r0
      jf 12
      call draw_test11_true
      ; dy != 0
        ; @test (dy > 0)
        isgt r8 r0
        jt 12
        call draw_test9_false
        ; dy > 0
          copy ra r7
          add ra r8
          ; @test (dx + dy > 0)
          isgt r8 r7
          jt draw_test7_true
          ; -dx >= dy
            copy r9 r7 ; (e <- dx)
            add r7 r7  ; (dx <- 2 * dx)
            add r8 r8  ; (dy <- 2 * dy)
            
            draw_boucle5_start:
              copy ra r4
              copy r4 r6
              syscall 2
              copy r4 ra
              sub r2 r1
              isequal r2 r4
              jf 12
              call draw_epilogue
              ; e <- e - dy
              add r9 r8
              isgt r0 r9
              jt draw_boucle5_start
              add r3 r1
              add r9 r7
              j draw_boucle5_start

          ; -dx < dy
          draw_test7_true:
            copy r9 r8 ; (e <- dy)
            add r7 r7  ; (dx <- 2 * dx)
            add r8 r8  ; (dy <- 2 * dy)

            draw_boucle6_start:
              copy ra r4
              copy r4 r6
              syscall 2
              copy r4 ra
              add r3 r1
              isequal r3 r5
              jf 12
              call draw_epilogue
              ; e <- e - dx
              add r9 r7
              isgt r9 r0
              jt draw_boucle6_start
              sub r2 r1
              add r9 r8
              j draw_boucle6_start

        ; dy < 0
        draw_test9_false:
          copy ra r8
          sub ra r7
          ; test (0 > (dy - dx))
          isgt r0 ra
          jt draw_test8_true
          ; dx <= dy
            copy r9 r7 ; (e <- dx)
            add r7 r7  ; (dx <- 2 * dx)
            add r8 r8  ; (dy <- 2 * dy)

            draw_boucle7_start:
              copy ra r4
              copy r4 r6
              syscall 2
              copy r4 ra
              sub r2 r1
              isequal r2 r4
              jf 12
              call draw_epilogue
              ; e <- e - dy
              sub r9 r8
              isgt r0 r9
              jt draw_boucle7_start
              sub r3 r1
              add r9 r7
              j draw_boucle7_start

          ; dx > dy
          draw_test8_true:
            copy r9 r8 ; (e <- dy)
            add r7 r7  ; (dx <- 2 * dx)
            add r8 r8  ; (dy <- 2 * dy)

            draw_boucle8_start:
              copy ra r4
              copy r4 r6
              syscall 2
              copy r4 ra
              sub r3 r1
              isequal r3 r5
              jf 12
              call draw_epilogue
              ; e <- e - dx
              sub r9 r7
              isgt r0 r9
              jt draw_boucle8_start
              sub r2 r1
              add r9 r8
              j draw_boucle8_start     

      ; dy == 0
      draw_test11_true:
        copy ra r4
        copy r4 r6
        syscall 2
        copy r4 ra
        sub r2 r1
        isequal r2 r4
        jf 12
        call draw_epilogue
        jf draw_test3_true         

  ; dx == 0
  draw_test1_true:
    ; dy <- y2 - y1
    copy r8 r5
    sub r8 r3
    ; @test (dy == 0)
    isequal r8 r0
    jf 12
    call draw_epilogue
    ; dy != 0
      ; @test (dy > 0)
      isgt r8 r0
      jf draw_test10_false
      ; dy > 0
        draw_boucle9_start:
          copy ra r4
          copy r4 r6
          syscall 2
          copy r4 ra
          add r3 r1
          isequal r3 r5
          jf draw_boucle9_start
          call draw_epilogue
      ; dy < 0
      draw_test10_false:
        draw_boucle1O_start:
          copy ra r4
          copy r4 r6
          syscall 2
          copy r4 ra
          sub r3 r1
          isequal r3 r5
          jf draw_boucle1O_start
          call draw_epilogue

draw_epilogue:

  ;; EPILOGUE
	mru8 ra rf
	mru8 r9 rf
	mru8 r8 rf
	mru8 r7 rf
	mru8 r3 rf
	mru8 r2 rf
	mru8 r0 rf