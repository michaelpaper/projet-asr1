;;; Le fichier contenant les routines systèmes (appels système)
;;; À la fin du fichier, les adresses des routines sont placées dans
;;; les adresses 0 à 1023 de la mémoire pour pouvoir être appelées
;;; directement avec des syscall

	ld8 rf 0xC4000 			; stack pointer for every routine in the program
	call boot

	;; prend en argument une couleur r2
	;; remplit l'écran avec la couleur représentée dans r2
clear_screen:
  #include clear_screen.s


	;; prend en argument les coordonnées (r2,r3) et une couleur r4
  ;; colorie le pixel de coordonnées (r2, r3) à la couleur représentée dans r4
plot:
  #include plot.s


	;; prend en argument les coordonnées (r2,r3), (r4,r5) et une couleur r6
  ;; colorie le rectangle formé par les coordonnées (r2, r3) et (r4, r5) à la couleur representée r6
fill:
  #include fill.s
  

  ;; prend en argument les coordonnées (r2,r3), (r4,r5) et une couleur r6
  ;; colorie le segment formé par les coordonnées (r2, r3) et (r4, r5) à la couleur representée r6
draw:
  #include draw.s


  ;; prend en argument les coordonnées (r2, r3), une couleur r4 et un code ASCII r5
  ;; dessine le caractère ASCII représenté par r5 aux coordonnées (r2, r3) et à la couleur r4
putchar:
  #include putchar.s

  ;; prend en argument les entiers signés r2 et r3, et calcule le produit et le stocke
  ;; dans le registre r4
multbis:
  #include multbis.s

  ;; prend en argument les entiers non signés r2 et r3, et calcule le produit et le stocke
  ;; dans le registre r4
mult:
  #include mult.s

  ;; Prend en entrée r2 et r3 deux nombres à virgule fixe (12.20) et calcule et stocke 
  ;; leur produit sur virgule fixe (12.20) dans r4
multfix:
  #include multfix.s

  ;; Portion du bootloader chargant en mémoire les adresses des routines systèmes dans leurs
  ;; cases réservées (les 1024 premières)
boot:
	copy r3 1
	shiftl3 r3
	ld8 r2 clear_screen
	mwu8 r2 r3
	ld8 r2 plot
	mwu8 r2 r3
	ld8 r2 fill
	mwu8 r2 r3
	ld8 r2 draw
	mwu8 r2 r3
  ld8 r2 putchar
  ld4 r4 0x0800
  add r2 r4      ;; On a besoin d'un offset car la police bitmap se trouve avant la procédure putchar
	mwu8 r2 r3
	ld8 r2 multbis
	mwu8 r2 r3
	ld8 r2 mult
	mwu8 r2 r3
	ld8 r2 multfix
	mwu8 r2 r3