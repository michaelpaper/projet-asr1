;; Prend en entrée r2 et r3 deux nombres à virgule fixe (7.24) et calcule et stocke 
;; leur produit sur virgule fixe (7.24) dans r4

;; Notes pour la multiplication sur 64 bits:
;;   X = E1 + (D1 << 16)
;;   Y = E2 + (D2 << 16)
;;   X*Y = E1*E2 + (E1*D2 + D2*E1 << 16) + (D1*D2 << 32)
;;   P = E1*E2 : sur 32 bits
;;   Q = D1*D2 : sur 32 bits
;;   R = (E1 + E2)(D1 + D2) : sur 32 bits

;; PROLOGUE
  push8 rf 0  ; save pcra
  push8 rf r2
  push8 rf r3
  push8 rf r5 ; P buffer
  push8 rf r6 ; Q buffer
  push8 rf r7 ; R buffer
  push8 rf r8 ; X buffer
  push8 rf r9 ; Y buffer
  push8 rf ra ; D1 buffer
  push8 rf rb ; D2 buffer
  push8 rf rc ; sign buffer

;; CORPS
  ;; Calcul du signe du résultat
  ld8 r4 0x80000000
  copy rc 0
  copy r5 r2
  copy r6 r3
  ;; Signe de X
  and r5 r4
  isequal r5 r4
  jf 13 
  copy rc 1
  not r2 r2
  add r2 1
  ;; Signe de Y
  and r6 r4
  isequal r6 r4
  jf 14
  xor rc 1
  not r3 r3
  add r3 1

  ;; Sauvegarde de |X| et |Y|
  copy r8 r2
  copy r9 r3

  ;; Calcul de P
  ld8 r4 0x0000FFFF
  and r2 r4
  and r3 r4
  syscall 7
  copy r5 r4

  ;; Calcul de D1 et D2
  copy ra r8
  copy rb r9
  ld8 r4 0xFFFF0000
  and ra r4
  and rb r4
  shiftr16 ra
  shiftr16 rb

  ;; Calcul de R
  add r2 ra
  add r3 rb
  syscall 7
  copy r7 r4

  ;; Calcul de Q
  copy r2 ra
  copy r3 rb
  syscall 7
  copy r6 r4

  ;; Reconstruction du résultat
  ;; R <- R - P - Q
  sub r7 r5
  sub r7 r6

  copy r2 r7
  ; ld8 r4 0x0000FFFF
  ; and r2 r4
  shiftl16 r2
  add r5 r2
  add r6 0  ; Ajout du bit potentiellement sorti de l'addition précédente
  copy r3 r7
  ; ld8 r4 0xFFFF0000
  ; and r3 r4
  shiftr16 r3
  add r6 r3

  ;; AJUSTEMENT DE LA VIRGULE FIXE ICI
  ;; P <- (P >> FS); Q <- (Q << (32 - FS))
  ;shiftr24 r5
  ;shiftr4 r5
  ;shiftr1 r5
  ;shiftl3 r6
  shiftr16 r5
  shiftr4 r5
  shiftr4 r5
  shiftl8 r6
  ;shiftl1 r6
  copy r4 r5
  add r4 r6

  ;; Application de la règle des signes
  isequal rc 0
  jt 10 
  not r4 r4
  add r4 1

  ;; Restauration de X et Y
  copy r2 r8
  copy r3 r9

;; EPILOGUE
  mru8 rc rf
  mru8 rb rf
  mru8 ra rf
  mru8 r9 rf
  mru8 r8 rf
  mru8 r7 rf
  mru8 r6 rf
  mru8 r5 rf
  mru8 r3 rf
  mru8 r2 rf
  mru8 r0 rf ; alternative ret
