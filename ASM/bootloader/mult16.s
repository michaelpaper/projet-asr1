;;; code pseudo-C :
;;; entrée : un registre 32bits contenant deux entiers non signés A et B de 16 bits chacun
;;; 1. A = r & 0x0000FFFF
;;; 2. B = r & 0xFFFF0000 >> 16
;;; 3. C = 0
;;; 4. while A != 0
;;; 5.   if (A&1 == 1) then C=C+B
;;; 6.   B = B << 1;
;;; 7.   A = A >> 1;
;;; 8. return C;

;;; r : r2
;;; A : r4
;;; B : r5
;;; C : r3

	copy r4 r2
	ld8 r6 0x0000FFFF
	and r4 r6 		; 1.
	copy r5 r2
	ld8 r6 0xFFFF0000
	and r5 r6
	shiftr16 r5		; 2.
	copy r3 0		; 3.
	isequal 0 r4
	jt 28			; 4.
	copy r6 r4
	and r6 1
	isequal r6 1
	jf 6
	add r3 r5		; 5.
	shiftl1 r5		; 6.
	shiftr1 r4		; 7.
	j -28

;;; La première ligne est toujours exécutée 1 fois
;;; La boucle principale est exécutée au plus 32 fois
;;; Le saut conditionnel à l'intérieur de la boucle est effectué au
;;; moins 0 fois
;;; Au total, on a donc, au pire cas, 42 + 31*32 + 6 = 1040 mioches de lus.
