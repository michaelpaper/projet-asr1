  ;; 8x8 bitmap utilisée pour afficher des caractères ASCII 
fontbitmap:
  #loadbin font.bitmap

  ;; prend en argument les coordonnées (r2, r3), une couleur r4 et un code ASCII r5
  ;; ARGS : x = r2, y = r3, color = r4, char = r5
  ;; PROLOGUE
  push8 rf 0  ; save pcra
  push8 rf r2 
  push8 rf r3
  push8 rf r4
  push8 rf r5
  push8 rf r6 ; code (bas)
  push8 rf r7 ; code (haut)
  push8 rf r8 ; cx
  push8 rf r9 ; cy
  push8 rf ra ; addr
  push8 rf rb ; const
  push8 rf rc ; counter
  push8 rf rd ; buffer

  ;; CORPS
  copy rd r4
  copy r4 r5
  copy r5 rd
  ld8 ra fontbitmap
  shiftl4 r5
  add ra r5
  mru8 r6 ra
  mru8 r7 ra

  copy r8 r2 ; original x conservation
  copy r9 r3 ; original y conservation
  ; ra is now used as a counter

  ; r5 is now used as a mask
  ld8 rb 0x00000004
  copy r5 r1
  copy r3 r9
  add r3 rb
  ld8 rb 0x00000004
  copy rc r0
high_row_loop_y:
  copy ra r0
  copy r2 r8
  ld2 rb 0x08
high_row_loop_x:
  copy rd r7
  and rd r5
  isequal rd r0
  jt high_row_loop_after_plot
  syscall 2
high_row_loop_after_plot:  
  add r2 r1
  shiftl1 r5
  add ra r1
  isequal ra rb
  jf high_row_loop_x
  add rc r1
  sub r3 r1
  ld2 ra 0x04
  isequal rc ra
  jf high_row_loop_y
  
  ; r5 is now used as a mask
  ld8 rb 0x00000008
  copy r5 r1
  copy r3 r9
  add r3 rb
  copy rc r0
low_row_loop_y:
  copy ra r0
  copy r2 r8
  ld2 rb 0x08
low_row_loop_x:
  copy rd r6
  and rd r5
  isequal rd r0
  jt low_row_loop_after_plot
  syscall 2
low_row_loop_after_plot:  
  add r2 r1
  shiftl1 r5
  add ra r1
  isequal ra rb
  jf low_row_loop_x
  add rc r1
  sub r3 r1
  ld2 ra 0x04
  isequal rc ra
  jf low_row_loop_y


  ;; EPILOGUE
  mru8 rd rf ; buffer
  mru8 rc rf ; counter
  mru8 rb rf ; const
  mru8 ra rf ; addr
  mru8 r9 rf ; cy
  mru8 r8 rf ; cx
  mru8 r7 rf ; code 2
  mru8 r6 rf ; code 1
  mru8 r5 rf
  mru8 r4 rf
  mru8 r3 rf
  mru8 r2 rf 
  mru8 r0 rf ; alternative ret