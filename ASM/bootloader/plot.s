	;; prend en argument les coordonnées (r2,r3) et une couleur r4
    ;; colorie le pixel de coordonnées (r2, r3) à la couleur représentée dans r4
	push8 rf r3
	push8 rf r5

	ld8 r5 255
	sub r5 r3
	copy r3 r5
	shiftl8 r5
	shiftl4 r3
	shiftl2 r3
	add r3 r5		; r3 = 320 * r3
	add r3 r2		; r3 = r3 + r2
	copy r5 r3
	add r3 r3
	add r3 r5		; r3 = 3*r3
	ld8 r5 0xC4000
	add r3 r5
	mw3 r4 r3
	
	mru8 r5 rf
	mru8 r3 rf
	ret