;; #include bootloader/boot.s

traitement_1:
  push8 rf ra
  push8 rf rb
  push8 rf r4
  
  ;; Calcul du vecteur de deplacement
  copy r2 r9
  copy r3 r7
  syscall 8
  copy ra r4
  copy r3 r8
  syscall 8
  copy rb r4

  copy r2 r5
  copy r3 r6
  copy r4 r2
  copy r5 r3
  add r4 ra
  add r5 rb
  mru8 r6 rf
  syscall 9

  copy r2 r4
  copy r3 r5
  copy r4 r6
  copy r5 r2
  copy r6 r3

  mru8 rb rf
  mru8 ra rf
  call debut_cycle

traitement_2:
  ;; RIEN à FAIRE
  call debut_cycle

traitement_3:
  push8 rf r4
  push8 rf r5
  push8 rf r6

  ;; Calcul cos(t+a)
  copy r2 r7
  copy r3 ra
  syscall 8
  copy r5 r4
  copy r2 r8
  copy r3 rb
  syscall 8
  sub r5 r4

  ;; Calcul sin(t+a)
  copy r2 r8
  copy r3 ra
  syscall 8
  copy r6 r4
  copy r2 rb
  copy r3 r7
  syscall 8
  add r6 r4

  copy r7 r5
  copy r8 r6

  mru8 r6 rf
  mru8 r5 rf
  mru8 r4 rf
  call debut_cycle

traitement_4:
  push8 rf r5
  push8 rf r6
  push8 rf r4

  ;; Calcul cos(t-a)
  copy r2 r7
  copy r3 ra
  syscall 8
  copy r5 r4
  copy r2 r8
  copy r3 rb
  syscall 8
  add r5 r4

  ;; Calcul sin(t-a)
  copy r2 r8
  copy r3 ra
  syscall 8
  copy r6 r4
  copy r2 rb
  copy r3 r7
  syscall 8
  sub r6 r4

  copy r7 r5
  copy r8 r6

  mru8 r6 rf
  mru8 r5 rf
  mru8 r4 rf
  call debut_cycle

traitement_5:
  ;; Calcul de cos(t + pi)
  not r7 r7
  add r7 1

  ;; Calcul de sin(t + pi)
  not r8 r8
  add r8 1

  call debut_cycle

traitement_6:
  mwu8 r5 re
  mwu8 r6 re
  mwu8 r7 re
  mwu8 r8 re
  call debut_cycle

traitement_7:
  mr8 r8 re
  push8 re 0
  mr8 r7 re
  push8 re 0
  mr8 r6 re
  push8 re 0
  mr8 r5 re
  push8 re 0
  call debut_cycle

debut_cycle:
  isequal rc 0
  jf 12
  call fin_cycle
  mru1 r2 rc

  ;; Exécution de F
  ld4 r3 0x1
  isequal r2 r3
  jf 12
  call traitement_1

  ;; Exécution de X
  ;; ld4 r3 0x2
  ;; isequal r2 r3
  ;; jf 12
  ;; call traitement_2

  ;; Exécution de +
  ld4 r3 0x3
  isequal r2 r3
  jf 12
  call traitement_3

  ;; Exécution de -
  ld4 r3 0x4
  isequal r2 r3
  jf 12
  call traitement_4

  ;; Exécution de |
  ld4 r3 0x5
  isequal r2 r3
  jf 12
  call traitement_5

  ;; Exécution de [
  ld4 r3 0x6
  isequal r2 r3
  jf 12
  call traitement_6

  ;; Exécution de ]
  ld4 r3 0x7
  isequal r2 r3
  jf 12
  call traitement_7

  add rc 1
  call debut_cycle

fin_cycle:
  ;; Eventuellement de l'affichage