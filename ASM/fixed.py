#!/usr/bin/env python3
from math import floor

INTEGRAL_SIZE   = 10
FRACTIONAL_SIZE = 22

def padded_hex(n, m):
    s = hex(n)[2:]
    return ("0" * (m - len(s))) + s

def fixed(x):
    """
    s = 0
    if (x < 0):
        x = -x
        s = 1
    ip = int(x)
    fp = x - ip; sp = fp
    fp *= (2 ** FRACTIONAL_SIZE)
    fp = int(fp)
    if (s == 1):
        ip += (2 ** FRACTIONAL_SIZE)
    print(s, ip, sp, fp, INTEGRAL_SIZE // 4, FRACTIONAL_SIZE // 4)
    return (padded_hex(ip, 3) +
            padded_hex(fp, FRACTIONAL_SIZE // 4))
    """
    n = int(x * (2 ** FRACTIONAL_SIZE))
    if (n <= 0):
        n += (2 ** 32)
    return padded_hex(n, 8)

def unfixed(s):
    if len(s) != 8:
        return None
    else:
        n = int(s, 16)
        if (n >= (2 ** 31)):
            n -= (2 ** 32)
        return (n / (2 ** FRACTIONAL_SIZE))
