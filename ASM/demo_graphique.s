#include bootloader/boot.s

main:
	copy r2 0
	copy r3 0
	ld4 r4 320
	ld4 r5 255
	ld4 r6 0xF00
	syscall 4

	ld4 r2 20
	ld4 r3 20
	ld4 r4 140
	ld4 r5 50
	ld4 r6 0x00F
	syscall 3

	ld4 r2 180
	ld4 r4 300
	ld4 r6 0x0F0
	syscall 3

	ld4 r2 40
	ld4 r3 40
	ld4 r4 89 		; Y
	ld4 r5 0xFFF
	syscall 5
	ld4 r4 97		; A
	ld4 r2 48
	syscall 5
	ld4 r4 110		; N
	ld4 r2 56
	syscall 5
	ld4 r4 110		; N
	ld4 r2 64
	syscall 5
	ld4 r4 105		; I
	ld4 r2 72
	syscall 5
	ld4 r4 115		; S
	ld4 r2 80
	syscall 5

	ld4 r4 77		; M
	ld4 r2 200
	syscall 5
	ld4 r4 105		; I
	ld4 r2 208
	syscall 5
	ld4 r4 99		; C
	ld4 r2 216
	syscall 5
	ld4 r4 104		; H
	ld4 r2 224
	syscall 5
	ld4 r4 97		; A
	ld4 r2 232
	syscall 5
	ld4 r4 101		; E
	ld4 r2 240
	syscall 5
	ld4 r4 108		; L
	ld4 r2 248
	syscall 5

	ld4 r2 40
	ld4 r3 25
	ld4 r4 79		; O
	syscall 5
	ld4 r4 78
	ld4 r2 48		; N
	syscall 5
	ld4 r4 68
	ld4 r2 56		; D
	syscall 5
	ld4 r4 65
	ld4 r2 64		; A
	syscall 5
	ld4 r4 82
	ld4 r2 72		; R
	syscall 5
	ld4 r4 83
	ld4 r2 80		; S
	syscall 5

	ld4 r2 200
	ld4 r4 80		; P
	syscall 5
	ld4 r2 208
	ld4 r4 65		; A
	syscall 5
	ld4 r2 216
	ld4 r4 80		; P
	syscall 5
	ld4 r2 224
	ld4 r4 69		; E
	syscall 5
	ld4 r2 232
	ld4 r4 82		; R
	syscall 5
